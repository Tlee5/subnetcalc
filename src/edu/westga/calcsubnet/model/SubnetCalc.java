package edu.westga.calcsubnet.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Defines a class that can calculate the subnet by being given an IP address
 * and netmask
 * 
 * @author tammylee
 * @version CS3280
 *
 */
public class SubnetCalc {

	/**
	 * Calculates the subnet after being given the IP address and number of mask
	 * bits
	 * 
	 * @param ipAddress
	 *            The IP address
	 * @param numMaskBits
	 *            The number of mask bits
	 * 
	 * @return The subnet for the given IP address and netmask
	 */
	public static String findSubnet(String ipAddress, int numMaskBits) {
		if (numMaskBits < 0 || numMaskBits > 32) {
			throw new IllegalArgumentException("Invalid number of mask bits");
		}
		int mask = numMaskBits / 8;
		int leftover = numMaskBits % 8;
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		map.put(1, 128);
		map.put(2, 192);
		map.put(3, 224);
		map.put(4, 240);
		map.put(5, 248);
		map.put(6, 252);
		map.put(7, 254);
		String subnetMask = "";
		if (mask == 0 && leftover == 0) {
			subnetMask += "0.0.0.0";
		}
		for (int i = 0; i < mask; i++) {
			if (i >= 1) {
				subnetMask += ".";
			}
			subnetMask += "255";
		}
		if (mask < 4 && leftover == 0) {
			int difference = 4 - mask;
			subnetMask = fillZeros(subnetMask, difference);
		}
		if (mask < 4 && leftover > 0) {
			if (mask > 0) {
				subnetMask += ".";
			}
			subnetMask += +map.get(leftover);
			if (mask + 1 < 4) {
				int difference = 4 - (mask + 1);
				subnetMask = fillZeros(subnetMask, difference);

			}
		}
		return SubnetCalc.findSubnet(ipAddress, subnetMask);
	}

	private static String fillZeros(String subnetMask, int difference) {
		for (int i = 0; i < difference; i++) {
			subnetMask += ".0";
		}
		return subnetMask;
	}

	/**
	 * Calculates the subnet after being given the IP address and the netmask
	 * 
	 * @precondition ipAddress != null AND subnetMask != null AND ipAddress !=
	 *               "" AND subnetMask != ""
	 * 
	 * @param ipAddress
	 *            The IP address
	 * 
	 * @param subnetMask
	 *            The subnet mask
	 * 
	 * @throws IllegalArgumentException
	 *             If subnetMask or ipAddress contain letters, if any octet of
	 *             the subnetMask or ipAddress are less than 0 or greater than
	 *             255, or if the given address or subnet contain an octet
	 *             greater than 3 digits in length, if subnet octets are not
	 *             divisible by 16, and if a subnet address has any number lower
	 *             than 255 proceeding a 255
	 * 
	 * 
	 * @return The subnet for the given IP address and netmask
	 * 
	 */

	public static String findSubnet(String ipAddress, String subnetMask) {
		if (ipAddress.equals("") || subnetMask.equals("")) {
			throw new IllegalArgumentException("IP address or subnet mask cannot be empty");
		}
		if (ipAddress.equals(null) || subnetMask.equals(null)) {
			throw new IllegalArgumentException("IP address or subnet mask cannot be null");
		}
		if (ipAddress.matches(".*[a-zA-Z]+.*") || subnetMask.matches(".*[a-zA-Z]+.*")) {
			throw new IllegalArgumentException("IP address or subnet mask must not contain letters");
		}
		Integer[] validMaskNumbers = { 0, 128, 192, 224, 240, 248, 252, 254, 255 };
		ArrayList<Integer> maskNumbers = new ArrayList<Integer>();
		maskNumbers.addAll(Arrays.asList(validMaskNumbers));
		String[] addressTokens = ipAddress.split("[.]");
		String[] maskTokens = subnetMask.split("[.]");
		if (addressTokens.length < 4 || addressTokens.length > 4) {
			throw new IllegalArgumentException("Invalid IP address");
		}
		if (maskTokens.length < 4 || maskTokens.length > 4) {
			throw new IllegalArgumentException("Invalid subnet mask");
		}
		String subnet = "";
		for (int i = 0; i < 4; i++) {
			if (Integer.parseInt(maskTokens[i]) < 0 || Integer.parseInt(maskTokens[i]) > 255) {
				throw new IllegalArgumentException("Invalid subnet mask");
			}
			if (Integer.parseInt(addressTokens[i]) < 0 || Integer.parseInt(addressTokens[i]) > 255) {
				throw new IllegalArgumentException("Invalid IP address");
			}
			if (!maskNumbers.contains(Integer.parseInt(maskTokens[i]))) {
				throw new IllegalArgumentException("Invalid mask number entered");
			}
			if (i < 3) {
				if (Integer.parseInt(maskTokens[i]) < 255 && Integer.parseInt(maskTokens[i + 1]) == 255) {
					throw new IllegalArgumentException("Invalid subnet");
				}
			}
			if (i >= 1) {
				subnet = subnet + ".";
			}
			if (maskTokens[i].equals("255")) {
				subnet = subnet + addressTokens[i];
			} else if (maskTokens[i].equals("0")) {
				subnet = subnet + "0";
			} else {
				int result = Integer.parseInt(addressTokens[i]) & Integer.parseInt(maskTokens[i]);
				subnet = subnet + result;
			}
		}
		return subnet;
	}

}
