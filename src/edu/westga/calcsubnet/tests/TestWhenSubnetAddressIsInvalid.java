package edu.westga.calcsubnet.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.calcsubnet.model.SubnetCalc;

/**
 * Tests invalid cases of subnet masks and IP address is valid
 * 
 * @author tammylee
 *
 */
public class TestWhenSubnetAddressIsInvalid {

	SubnetCalc calc;

	/**
	 * Sets up everything necessary to run tests
	 * 
	 * @throws Exception
	 *             This should not happen
	 */

	@Before
	public void setUp() throws Exception {
		this.calc = new SubnetCalc();
	}

	/**
	 * Tests that a valid IP address but subnet mask with letters throws
	 * exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionSubnetMaskHasLettersWithValidIpAddress() {
		SubnetCalc.findSubnet("160.10.25.6", "abc");
	}

	/**
	 * Tests that a valid IP address but mask bit is negative throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionMaskBitIsNegativeWithValidIpAddress() {
		SubnetCalc.findSubnet("160.10.25.6", -1);
	}

	/**
	 * Tests that a valid IP address but subnet mask is null throws exception
	 */
	@Test(expected = NullPointerException.class)
	public void testThrowsExceptionSubnetMaskIsNullWithValidIpAddress() {
		SubnetCalc.findSubnet("160.10.25.6", null);
	}

	/**
	 * Tests that a valid IP address but subnet mask is empty throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionSubnetMaskIsEmptyWithValidIpAddress() {
		SubnetCalc.findSubnet("160.10.25.6", "");
	}

	/**
	 * Tests that a valid IP address but subnet mask exceeds 32 mask bit throws
	 * exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionMaskBitExceedsThirtyTwoBitsWithValidIpAddress() {
		SubnetCalc.findSubnet("160.10.25.6", 40);
	}

	/**
	 * Tests that a valid IP address but subnet mask with number larger that 255
	 * throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionSubnetMaskExceedsTwoHundredAndFiftyFiveWithValidIpAddress() {
		SubnetCalc.findSubnet("160.10.25.6", "255.480.0.0");
	}
	
	/**
	 * Tests that a valid IP address but subnet mask of 108.255.255.0 throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionSubnetMaskHasInvalidNumberInFrontWithValidIpAddress(){
		SubnetCalc.findSubnet("160.10.25.6", "108.255.255.0");
	}
	
	/**
	 * Tests that a valid IP address but subnet mask of 255.192.255.0 throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionSubnetMaskHasInvalidNumberInMiddleWithValidIpAddress(){
		SubnetCalc.findSubnet("160.10.25.6", "255.192.255.0");
	}
	
	/**
	 * Tests that a valid IP address but subnet mask of 192.255.255.0 throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionSubnetMaskHasInvalidNumberAtEndWithValidIpAddress(){
		SubnetCalc.findSubnet("160.10.25.6", "255.255.192.255");
	}
	
	/**
	 * Tests that a valid IP address but invalid subnet mask of 255.255.13.0 throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionSubnetMaskHasInvalidNumberValidIpAddress(){
		SubnetCalc.findSubnet("160.10.25.6", "255.255.13.0");
	}
	
	/**
	 * Tests that a valid IP address but invalid subnet mask of 255.255.0 throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionSubnetMaskIsTooShort(){
		SubnetCalc.findSubnet("160.10.25.6", "255.255.0");
	}
	
	/**
	 * Tests that a valid IP address but invalid subnet mask of 255.255.0.0.0 throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionSubnetMaskIsTooLong(){
		SubnetCalc.findSubnet("160.10.25.6", "255.255.0.0.0");
	}
	
	/**
	 * Tests that a valid IP address but subnet mask of one over max boundary throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionMaskBitOneOverMaxBoundary(){
		SubnetCalc.findSubnet("160.10.25.6", 33);
	}
	/**
	 * Tests that valid IP address and subnet mask with five below boundary throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionMaskBitFiveBelowMinBoundary(){
		SubnetCalc.findSubnet("160.10.25.6", -5);
	}
}
