package edu.westga.calcsubnet.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.calcsubnet.model.SubnetCalc;

/**
 * This class tests when the findSubnet method that accepts a mask string when
 * the IP address and netmask are valid
 * 
 * @author tammylee
 * @version CS3280
 *
 */
public class TestWhenIpAddressAndSubnetAreValidWithMaskString {

	SubnetCalc calc;

	/**
	 * Sets up everything necessary to run tests
	 * 
	 * @throws Exception
	 *             This should not happen
	 */

	@Before
	public void setUp() throws Exception {
		this.calc = new SubnetCalc();
	}

	/**
	 * Tests that the IP address of 160.10.25.6 and netmask of 255.255.255.0
	 * return the subnet of 160.10.25.0
	 */
	@Test
	public void testNetmaskOfTwentyFourMaskBits() {
		String subnet = SubnetCalc.findSubnet("160.10.25.6", "255.255.255.0");
		assertEquals("160.10.25.0", subnet);
	}

	/**
	 * Tests that the IP address of 160.10.25.6 and netmask of 255.255.255.255
	 * return the subnet of 160.10.25.6
	 */
	@Test
	public void testNetmaskOfThirtyTwoMaskBits() {
		String subnet = SubnetCalc.findSubnet("160.10.25.6", "255.255.255.255");
		assertEquals("160.10.25.6", subnet);
	}

	/**
	 * Tests that the IP address of 160.10.25.6 and netmask of 255.255.0.0
	 * returns the subnet of 160.10.0.0.
	 */
	@Test
	public void testNetmaskOfSixteenMaskBits() {
		String subnet = SubnetCalc.findSubnet("160.10.25.6", "255.255.0.0");
		assertEquals("160.10.0.0", subnet);
	}

	/**
	 * Tests that the IP address of 160.10.15.6 and netmask of 255.0.0.0 returns
	 * the subnet of 160.0.0.0
	 */
	@Test
	public void testNetmaskOfEightMaskBits() {
		String subnet = SubnetCalc.findSubnet("160.10.25.6", "255.0.0.0");
		assertEquals("160.0.0.0", subnet);
	}

	/**
	 * Tests that the IP address of 160.10.15.6 and netmask of 0.0.0.0 returns
	 * the subnet of 0.0.0.0
	 */
	@Test
	public void testNetmaskOfZeroMaskBits() {
		String subnet = SubnetCalc.findSubnet("160.10.25.6", "0.0.0.0");
		assertEquals("0.0.0.0", subnet);
	}

	/**
	 * Tests that the IP address of 160.10.15.6 and netmask of 192.0.0.0 returns
	 * the subnet of 128.0.0.0
	 */
	@Test
	public void testNetmaskOfOneHundredAndNinetyTwoFollowedByZeros() {
		String subnet = SubnetCalc.findSubnet("160.10.15.6", "192.0.0.0");
		assertEquals("128.0.0.0", subnet);
	}

	/**
	 * Tests that the IP address of 160.10.15.6 and netmask of 255.240.0.0
	 * returns the subnet of 160.0.0.0
	 */
	@Test
	public void testNetmaskOfTwoHundredAndFiftyFiveFollowedByTwoHundredAndFortyThenZeros() {
		String subnet = SubnetCalc.findSubnet("160.10.15.6", "255.240.0.0");
		assertEquals("160.0.0.0", subnet);
	}

	/**
	 * Tests that the IP address of 160.10.15.6 and netmask of 255.255.192.0
	 * returns the subnet of 160.10.0.0
	 */
	@Test
	public void testNetmaskOfTwoHundredFiftyFiveTwiceFollowedByOneHundredAndNinetyTwoThenZeros() {
		String subnet = SubnetCalc.findSubnet("160.10.15.6", "255.255.192.0");
		assertEquals("160.10.0.0", subnet);
	}

	/**
	 * Tests that the IP address of 160.10.15.6 and the netmask of
	 * 255.255.255.128 returns the subnet of 160.10.15.0
	 */
	@Test
	public void testNetmaskOfTwoHundredFiftyFiveThreeTimesFollowedByOneHundredAndTwentyEight() {
		String subnet = SubnetCalc.findSubnet("160.10.15.6", "255.255.255.128");
		assertEquals("160.10.15.0", subnet);
	}
}
