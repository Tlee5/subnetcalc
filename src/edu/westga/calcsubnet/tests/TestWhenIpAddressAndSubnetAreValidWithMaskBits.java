package edu.westga.calcsubnet.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.calcsubnet.model.SubnetCalc;

/**
 * This class tests the findSubnet method that accepts mask bits when a correct
 * IP address and subnet are applied
 * 
 * @author tammylee
 * @version CS3280
 *
 */
public class TestWhenIpAddressAndSubnetAreValidWithMaskBits {

	SubnetCalc calc;

	/**
	 * Sets up everything necessary to run tests
	 * 
	 * @throws Exception
	 *             This should not happen
	 */

	@Before
	public void setUp() throws Exception {
		this.calc = new SubnetCalc();
	}

	/**
	 * Tests that the IP address of 160.10.25.6 and mask bit of 32 create the
	 * subnet of 160.10.25.6
	 */
	public void testWhenSubnetMaskBitIsThirtyTwo() {
		String subnet = SubnetCalc.findSubnet("160.10.25.6", 32);
		assertEquals("160.10.25.6", subnet);
	}

	/**
	 * Tests that the IP address of 160.10.25.6 and mask bit of 24 create the
	 * subnet of 160.10.25.0
	 */
	@Test
	public void testWhenSubnetMaskBitIsTwentyFour() {
		String subnet = SubnetCalc.findSubnet("160.10.25.6", 24);
		assertEquals("160.10.25.0", subnet);
	}

	/**
	 * Tests that IP address of 160.10.25.6 and mask bit of 16 create the subnet
	 * of 160.10.0.0
	 */
	@Test
	public void testWhenSubnetMaskBitIsSixteen() {
		String subnet = SubnetCalc.findSubnet("160.10.25.6", 16);
		assertEquals("160.10.0.0", subnet);
	}

	/**
	 * Tests that IP address of 160.10.25.6 and mask bit of 8 create the subnet
	 * of 160.0.0.0
	 */
	@Test
	public void testWhenSubnetMaskBitIsEight() {
		String subnet = SubnetCalc.findSubnet("160.10.25.6", 8);
		assertEquals("160.0.0.0", subnet);
	}

	/**
	 * Tests that IP address of 160.10.25.6 and mask bit of 0 create the subnet
	 * of 0.0.0.0
	 */
	@Test
	public void testWhenSubnetMaskBitIsZero() {
		String subnet = SubnetCalc.findSubnet("160.10.25.6", 0);
		assertEquals("0.0.0.0", subnet);
	}

	/**
	 * Tests that IP address of 160.10.25.6 and mask bit of 5 create the subnet
	 * of 160.0.0.0
	 */
	@Test
	public void testWhenSubnetMaskBitIsFive() {
		String subnet = SubnetCalc.findSubnet("160.10.25.6", 5);
		assertEquals("160.0.0.0", subnet);
	}
	
	/**
	 * Tests that IP address of 160.10.25.6 and mask bit of 13 create the subnet of 160.8.0.0 
	 */
	@Test
	public void testWhenSubnetMaskBitIsTwelve(){
		String subnet = SubnetCalc.findSubnet("160.10.25.6", 13);
		assertEquals("160.8.0.0", subnet);
	}
	
	/**
	 * Tests that IP address of 160.10.25.6 and mask bit of 19 create the subnet of 160.10.0.0
	 */
	@Test
	public void testWhenSubnetMaskBitIsNineteen(){
		String subnet = SubnetCalc.findSubnet("160.10.25.6", 19);
		assertEquals("160.10.0.0", subnet);
	}
	
	/**
	 * Tests that IP address of 160.10.25.6 and mask bit of 27 create the subnet of 160.10.25.0
	 */
	@Test
	public void testWhenSubnetMaskBitIsTwentySeven(){
		String subnet = SubnetCalc.findSubnet("160.10.25.6", 27);
		assertEquals("160.10.25.0", subnet);
	}
}
