package edu.westga.calcsubnet.tests;

import org.junit.Before;
import org.junit.Test;

import edu.westga.calcsubnet.model.SubnetCalc;

/**
 * Tests invalid cases of IP addresses but subnet is valid
 * 
 * @author tammylee
 * @version CS3280
 */

public class TestWhenIPAddressIsInvalid {

	SubnetCalc calc;

	/**
	 * Sets up everything necessary to run tests
	 * 
	 * @throws Exception
	 *             This should not happen
	 */

	@Before
	public void setUp() throws Exception {
		this.calc = new SubnetCalc();
	}

	/**
	 * Tests that an IP address with letters and a valid submask string throws
	 * exception
	 */

	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForLettersWithValidSubmaskString() {
		SubnetCalc.findSubnet("abc", "255.255.0.0");
	}

	/**
	 * Tests that an IP address with letters and a valid mask bit throws
	 * exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForLettersWithValidMaskBits() {
		SubnetCalc.findSubnet("abc", 24);
	}

	/**
	 * Tests that an IP address that is null and a valid submask string throws
	 * exception
	 */

	@Test(expected = NullPointerException.class)
	public void testThrowsExceptionForNullIpAddressWithValidSubmaskString() {
		SubnetCalc.findSubnet(null, "255.255.255.0");
	}

	/**
	 * Tests that an IP address that is null and a valid mask bit throws
	 * exception
	 */
	@Test(expected = NullPointerException.class)
	public void testThrowsExceptionForNullIpAddressWithValidMaskBit() {
		SubnetCalc.findSubnet(null, 24);
	}

	/**
	 * Tests that an empty IP address and a valid submask string throws
	 * exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForEmptyIpAddressWithValidSubmaskString() {
		SubnetCalc.findSubnet("", "255.255.0.0");
	}
	
	/**
	 * Tests that an empty IP address and a valid mask bit throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForEmptyIpAddressWithValidMaskBit(){
		SubnetCalc.findSubnet("", 8);
	}
	
	/**
	 * Tests that a negative IP address and valid submask string throws
	 * exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForNegativeIpAddressWithValidSubmaskString() {
		SubnetCalc.findSubnet("-160.10.25.6", "255.255.0.0");
	}

	/**
	 * Tests that a negative IP address and valid mask bit throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForNegativeIpAddressWithValidMaskBit() {
		SubnetCalc.findSubnet("-160.10.25.6", 24);
	}

	/**
	 * Tests that an incomplete IP address and valid submask string throws
	 * exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForIncompleteIpAddressAndValidSubmaskString() {
		SubnetCalc.findSubnet("160.10.25", "192.0.0.0");
	}

	/**
	 * Tests than an incomplete IP address and valid mask bit throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForIncompleteIpAddressAndValidMaskBit() {
		SubnetCalc.findSubnet("160.10.25", 8);
	}

	/**
	 * Tests that a IP address with numbers larger than 255 and valid submask
	 * string throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForIpAddressThatContainsNumberLargerThanTwoHundredAndFiftyFiveWithValidSubmaskString() {
		SubnetCalc.findSubnet("192.485.3.0", "255.255.0.0");
	}

	/**
	 * Tests that a IP address with numbers larger than 255 and valid mask bit
	 * throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForIpAddressThatContainsNumberLargerThanTwoHundereAndFiftyFiveWithValidMaskBit(){
		SubnetCalc.findSubnet("495.23.10.4", 0);
	}
	
	/**
	 * Tests that a IP address with too many numbers and valid submask string
	 * throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForIpAddressLengthIsTooLongWithValidSubmaskString(){
		SubnetCalc.findSubnet("160.10.25.6.0", "255.255.255.0");
	}
	
	/**
	 * Tests that a IP address with too many numbers and valid mask bit
	 * throws exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testThrowsExceptionForIpAddressLengthIsTooLongWithValidMaskBit(){
		SubnetCalc.findSubnet("160.10.25.6.0", 24);
	}
	
}
